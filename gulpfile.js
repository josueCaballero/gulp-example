
'user stric';

const gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	bs = require('browser-sync').create();
var js = require('gulp-json-srv');


/* Options
================================================*/

opt = {

	"stylus": {

		"compress": true

	}

}

/* Tasks
================================================*/

gulp.task('bs', function (){

	bs.init({

		server: { baseDir: './public/' },

	});

	gulp.watch("public/**/*.+(html|css|js)", ()=>{}).on('change', () => {

		bs.reload();

	});

});

gulp.task('apiServer', () => {

	serverOpts = {

		"baseUrl": "/api",
		"port": 8080

	}

	let server = js.create(serverOpts);

	return gulp.src("./db/database.json").pipe(server.pipe());

});

gulp.task('default', [ "bs", "apiServer" ]);

/* Watchers
================================================*/

gulp.watch("src/js/*.js", () => {

	gulp.src([ "src/js/*.js", "node_modules/jquery/dist/jquery.min.js" ])
		.pipe(gulp.dest("public/js/"));

});

gulp.watch("src/css/*.styl", () => {
	
	gulp.src("src/css/*.styl")
		.pipe(stylus(opt.stylus))
		.pipe(gulp.dest("public/css/"));

});

