
$(() => {

	let section = $(".notesContainer"),
		btnAdd = $(".add:first"),
		btnDel = $(".del");

	let notesEmmiter = {},
		cNotesEmmiter = {};

	_.extend(notesEmmiter, Backbone.Events);
	_.extend(cNotesEmmiter, Backbone.Events);

	notesEmmiter.on("addNote", (e) => {

		let newNote = document.createElement("article");
		
		newNote.setAttribute("contenteditable", "true");
		newNote.setAttribute("class", "note");
		section.prepend(newNote);

	});

	notesEmmiter.on("delNote", (e) => {

		$(".note:first").remove();
		
	});

	cNotesEmmiter.listenTo(notesEmmiter, "addNote", (e) => console.log(e.val()));

	// Hay que enviar los eventos con function en lugar de () => {} si se desea enviar el objeto $(this)
	btnAdd.click(function () {notesEmmiter.trigger("addNote", $(this))});
	btnDel.click(function () {notesEmmiter.trigger("delNote", $(this))});


	let Libro = Backbone.Model.extend(

		{ // Elementos de instancia

			"ISBN": "un numero con giones",
			"defaults": {

				"nombre": null,
				"editorial": null
			
			},

			"initialize": function (attr){

				this.on("change:nombre", this.onChangeName, this);

				console.log("Se ha creado un nuevo objeto con nombre " + attr.nombre);
				this.set("editorial", this.get("editorial") + " " + attr.algo);

			},

			"toString": function (){

				console.log(`La editorial es ${this.attributes.editorial}.\
					\nEl ISBN es ${this.ISBN}\
					\nEl nombre es ${this.get("nombre")}`
				);

			},

			"onChangeName": function (model, opts){

				console.log(`Se ha canbiado el nombre a ${this.get("nombre")}`);
				console.log(model);

			}

		},

		{ // Elementos staticos

			propiedad: "La vida nueva!"

		}

	);

	let jhtp7 = new Libro({"nombre": "Java How to program 7 ED", "editorial": "Pearson Education", "algo": "more"});
	console.log(jhtp7.toString());
	console.log(jhtp7.toJSON());
	jhtp7.set("nombre", "Como programar en Java 7Ed");

	console.log(Libro.propiedad);

});
